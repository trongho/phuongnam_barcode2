package com.symbol.barcodesample1.Model;

import java.util.Date;

public class ExportBook {
    private String coilNo;
    private double size;
    private double netWeight;
    private double grossWeight;
    private double length;
    private String qualityStandard;
    private String classification;
    private String coatingMass;
    private String property;
    private String surfaceTreatment;
    private Date dateOfProduction;
    private String inspector;

    public ExportBook(String coilNo, double size, double netWeight, double grossWeight, double length, String qualityStandard, String classification, String coatingMass, String property, String surfaceTreatment, Date dateOfProduction, String inspector) {
        this.coilNo = coilNo;
        this.size = size;
        this.netWeight = netWeight;
        this.grossWeight = grossWeight;
        this.length = length;
        this.qualityStandard = qualityStandard;
        this.classification = classification;
        this.coatingMass = coatingMass;
        this.property = property;
        this.surfaceTreatment = surfaceTreatment;
        this.dateOfProduction = dateOfProduction;
        this.inspector = inspector;
    }

    public ExportBook() {
    }

    public String getCoilNo() {
        return coilNo;
    }

    public void setCoilNo(String coilNo) {
        this.coilNo = coilNo;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public double getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(double netWeight) {
        this.netWeight = netWeight;
    }

    public double getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(double grossWeight) {
        this.grossWeight = grossWeight;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public String getQualityStandard() {
        return qualityStandard;
    }

    public void setQualityStandard(String qualityStandard) {
        this.qualityStandard = qualityStandard;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getCoatingMass() {
        return coatingMass;
    }

    public void setCoatingMass(String coatingMass) {
        this.coatingMass = coatingMass;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getSurfaceTreatment() {
        return surfaceTreatment;
    }

    public void setSurfaceTreatment(String surfaceTreatment) {
        this.surfaceTreatment = surfaceTreatment;
    }

    public Date getDateOfProduction() {
        return dateOfProduction;
    }

    public void setDateOfProduction(Date dateOfProduction) {
        this.dateOfProduction = dateOfProduction;
    }

    public String getInspector() {
        return inspector;
    }

    public void setInspector(String inspector) {
        this.inspector = inspector;
    }
}
