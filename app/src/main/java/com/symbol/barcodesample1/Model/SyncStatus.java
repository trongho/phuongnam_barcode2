package com.symbol.barcodesample1.Model;

public enum SyncStatus {
    SYNCED,
    SYNCFAIL,
    NOTSYNC,
}
