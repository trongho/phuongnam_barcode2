package com.symbol.barcodesample1.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ParseException;

import com.symbol.barcodesample1.Util.DBHelper;
import com.symbol.barcodesample1.Model.SessionDetail;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class SessionDetailDAO {
    DBHelper dbHelper;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public SessionDetailDAO(Context context){
        dbHelper=new DBHelper(context);
    }

    public ArrayList<SessionDetail> getAll() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<SessionDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM SESSION_DETAIL";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                int id=c.getInt(0);
                int sessionNumber = c.getInt(1);
                String coilNo = c.getString(2);
                double size=c.getDouble(3);
                double netWeight=c.getDouble(4);
                double grossWeight=c.getDouble(5);
                double length=c.getDouble(6);
                String qualityStandard = c.getString(7);
                String classification = c.getString(8);
                String coatingMass = c.getString(9);
                String property = c.getString(10);
                String surfaceTreatment = c.getString(11);
                Date dateOfProduction = simpleDateFormat.parse(c.getString(12));
                String inspector = c.getString(13);
                list.add(new SessionDetail(id,sessionNumber,coilNo,size,netWeight,grossWeight,length, qualityStandard, classification, coatingMass, property, surfaceTreatment,dateOfProduction, inspector));
            } catch (ParseException | java.text.ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public String getLastID() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String id="0";
        String sql = "SELECT MAX(ID_SESSION_DETAIL) FROM SESSION_DETAIL";
        Cursor c = db.rawQuery(sql,null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                id= String.valueOf(c.getInt(0));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return id;
    }

    public ArrayList<SessionDetail> getSortAscending(int idSession) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<SessionDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM SESSION_DETAIL WHERE ID_SESSION=? ORDER BY NET_WEIGHT ASC";
        Cursor c = db.rawQuery(sql, new String[]{String.valueOf(idSession)});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                int id=c.getInt(0);
                int sessionNumber = c.getInt(1);
                String coilNo = c.getString(2);
                double size=c.getDouble(3);
                double netWeight=c.getDouble(4);
                double grossWeight=c.getDouble(5);
                double length=c.getDouble(6);
                String qualityStandard = c.getString(7);
                String classification = c.getString(8);
                String coatingMass = c.getString(9);
                String property = c.getString(10);
                String surfaceTreatment = c.getString(11);
                Date dateOfProduction = simpleDateFormat.parse(c.getString(12));
                String inspector = c.getString(13);
                list.add(new SessionDetail(id,sessionNumber,coilNo,size,netWeight,grossWeight,length, qualityStandard, classification, coatingMass, property, surfaceTreatment,dateOfProduction, inspector));
            } catch (ParseException | java.text.ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<SessionDetail> getSortABC(int idSession) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<SessionDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM SESSION_DETAIL WHERE ID_SESSION=? ORDER BY COIL_NO ASC";
        Cursor c = db.rawQuery(sql,new String[]{String.valueOf(idSession)});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                int id=c.getInt(0);
                int sessionNumber = c.getInt(1);
                String coilNo = c.getString(2);
                double size=c.getDouble(3);
                double netWeight=c.getDouble(4);
                double grossWeight=c.getDouble(5);
                double length=c.getDouble(6);
                String qualityStandard = c.getString(7);
                String classification = c.getString(8);
                String coatingMass = c.getString(9);
                String property = c.getString(10);
                String surfaceTreatment = c.getString(11);
                Date dateOfProduction = simpleDateFormat.parse(c.getString(12));
                String inspector = c.getString(13);
                list.add(new SessionDetail(id,sessionNumber,coilNo,size,netWeight,grossWeight,length, qualityStandard, classification, coatingMass, property, surfaceTreatment,dateOfProduction, inspector));
            } catch (ParseException | java.text.ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<SessionDetail> getSortDescending(int idSession) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<SessionDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM SESSION_DETAIL WHERE ID_SESSION=? ORDER BY NET_WEIGHT DESC";
        Cursor c = db.rawQuery(sql, new String[]{String.valueOf(idSession)});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                int id=c.getInt(0);
                int sessionNumber = c.getInt(1);
                String coilNo = c.getString(2);
                double size=c.getDouble(3);
                double netWeight=c.getDouble(4);
                double grossWeight=c.getDouble(5);
                double length=c.getDouble(6);
                String qualityStandard = c.getString(7);
                String classification = c.getString(8);
                String coatingMass = c.getString(9);
                String property = c.getString(10);
                String surfaceTreatment = c.getString(11);
                Date dateOfProduction = simpleDateFormat.parse(c.getString(12));
                String inspector = c.getString(13);
                list.add(new SessionDetail(id,sessionNumber,coilNo,size,netWeight,grossWeight,length, qualityStandard, classification, coatingMass, property, surfaceTreatment,dateOfProduction, inspector));
            } catch (ParseException | java.text.ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<SessionDetail> getSortASCID(int idSession) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<SessionDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM SESSION_DETAIL WHERE ID_SESSION=? ORDER BY ID_SESSION_DETAIL ASC";
        Cursor c = db.rawQuery(sql, new String[]{String.valueOf(idSession)});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                int id=c.getInt(0);
                int sessionNumber = c.getInt(1);
                String coilNo = c.getString(2);
                double size=c.getDouble(3);
                double netWeight=c.getDouble(4);
                double grossWeight=c.getDouble(5);
                double length=c.getDouble(6);
                String qualityStandard = c.getString(7);
                String classification = c.getString(8);
                String coatingMass = c.getString(9);
                String property = c.getString(10);
                String surfaceTreatment = c.getString(11);
                Date dateOfProduction = simpleDateFormat.parse(c.getString(12));
                String inspector = c.getString(13);
                list.add(new SessionDetail(id,sessionNumber,coilNo,size,netWeight,grossWeight,length, qualityStandard, classification, coatingMass, property, surfaceTreatment,dateOfProduction, inspector));
            } catch (ParseException | java.text.ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<SessionDetail> getBySession(int idSession) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<SessionDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM SESSION_DETAIL WHERE ID_SESSION=? ";
        Cursor c = db.rawQuery(sql, new String[]{String.valueOf(idSession)});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                int id=c.getInt(0);
                int sessionNumber = c.getInt(1);
                String coilNo = c.getString(2);
                double size=c.getDouble(3);
                double netWeight=c.getDouble(4);
                double grossWeight=c.getDouble(5);
                double length=c.getDouble(6);
                String qualityStandard = c.getString(7);
                String classification = c.getString(8);
                String coatingMass = c.getString(9);
                String property = c.getString(10);
                String surfaceTreatment = c.getString(11);
                Date dateOfProduction = simpleDateFormat.parse(c.getString(12));
                String inspector = c.getString(13);
                list.add(new SessionDetail(id,sessionNumber,coilNo,size,netWeight,grossWeight,length, qualityStandard, classification, coatingMass, property, surfaceTreatment,dateOfProduction, inspector));
            } catch (ParseException | java.text.ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public Boolean checkDuplicateCoilNo(String id,String sku) {
        Boolean flag = false;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "SELECT * FROM SESSION_DETAIL WHERE ID_SESSION=? AND COIL_NO=?";
        Cursor c = db.rawQuery(sql, new String[]{id,sku});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            if (c != null && c.getCount() > 0) {
                flag = true;
                break;
            }
            c.moveToNext();
        }
        return flag;
    }

    public long insert(SessionDetail sessionDetail) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("ID_SESSION", sessionDetail.getSession_id());
        values.put("COIL_NO", sessionDetail.getCoilNo());
        values.put("SIZE", sessionDetail.getSize());
        values.put("NET_WEIGHT", sessionDetail.getNetWeight());
        values.put("GROSS_WEIGHT", sessionDetail.getGrossWeight());
        values.put("LENGTH", sessionDetail.getLength());
        values.put("QUALITY_STANDARD", sessionDetail.getQualityStandard());
        values.put("CLASSIFICATION", sessionDetail.getClassification());
        values.put("COATING_MASS", sessionDetail.getCoatingMass());
        values.put("PROPERTY", sessionDetail.getProperty());
        values.put("SURFACE_TREATMENT", sessionDetail.getSurfaceTreatment());
        values.put("DATE_OF_PRODUCTION", simpleDateFormat.format(sessionDetail.getDateOfProduction()));
        values.put("INSPECTOR", sessionDetail.getInspector());
        return db.insert("SESSION_DETAIL", null, values);
    }


    public int updateByID(String id,SessionDetail sessionDetail) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("ID_SESSION", sessionDetail.getSession_id());
        values.put("COIL_NO", sessionDetail.getCoilNo());
        values.put("SIZE", sessionDetail.getSize());
        values.put("NET_WEIGHT", sessionDetail.getNetWeight());
        values.put("GROSS_WEIGHT", sessionDetail.getGrossWeight());
        values.put("LENGTH", sessionDetail.getLength());
        values.put("QUALITY_STANDARD", sessionDetail.getQualityStandard());
        values.put("CLASSIFICATION", sessionDetail.getClassification());
        values.put("COATING_MASS", sessionDetail.getCoatingMass());
        values.put("PROPERTY", sessionDetail.getProperty());
        values.put("SURFACE_TREATMENT", sessionDetail.getSurfaceTreatment());
        values.put("DATE_OF_PRODUCTION", simpleDateFormat.format(sessionDetail.getDateOfProduction()));
        values.put("INSPECTOR", sessionDetail.getInspector());
        return db.update("SESSION_DETAIL", values, "ID_SESSION_DETAIL=?", new String[]{String.valueOf(id)});
    }

    public int delete(SessionDetail sessionDetail) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("SESSION_DETAIL", "ID_SESSION=? AND COIL_NO=?", new String[]{String.valueOf(sessionDetail.getId()),sessionDetail.getCoilNo()});
    }

    public int delete(int idSection) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("SESSION_DETAIL", "ID_SESSION=?", new String[]{String.valueOf(idSection)});
    }

    public int deleteByID(String id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("SESSION_DETAIL", "ID_SESSION_DETAIL=?", new String[]{String.valueOf(id)});
    }

    public int deleteAll() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("SESSION_DETAIL",null,null);
    }
}
