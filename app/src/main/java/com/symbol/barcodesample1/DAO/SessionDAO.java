package com.symbol.barcodesample1.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.symbol.barcodesample1.Util.DBHelper;
import com.symbol.barcodesample1.Model.Session;
import com.symbol.barcodesample1.Model.SyncStatus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class SessionDAO {
    DBHelper dbHelper;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public SessionDAO(Context context) {
        dbHelper = new DBHelper(context);
    }

    public ArrayList<Session> getAll() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<Session> list = new ArrayList<>();
        String sql = "SELECT * FROM SESSION";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                String sessionNumber = c.getString(1);
                String deviceNumber = c.getString(2);
                int sumSku = c.getInt(3);
                Date createDate = simpleDateFormat.parse(c.getString(4));
                SyncStatus syncStatus= SyncStatus.valueOf(c.getString(5));
                list.add(new Session(deviceNumber, sessionNumber, sumSku,createDate,syncStatus));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public Session getSessionByID(int id){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Session session = null;
        String sql = "SELECT * FROM SESSION";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                String sessionNumber = c.getString(1);
                String deviceNumber = c.getString(2);
                int sumSku = c.getInt(3);
                Date createDate = simpleDateFormat.parse(c.getString(4));
                SyncStatus syncStatus= SyncStatus.valueOf(c.getString(5));
                session=new Session(deviceNumber,sessionNumber,sumSku,createDate,syncStatus);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return session;
    }

    public Session getSession(String sessionNumber1,String deviceNumber1){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Session session = null;
        String sql = "SELECT * FROM SESSION WHERE SESSION_NUMBER=? AND DEVICE_NUMBER=?";
        Cursor c = db.rawQuery(sql, new String[]{String.valueOf(sessionNumber1),deviceNumber1});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                String sessionNumber = c.getString(1);
                String deviceNumber = c.getString(2);
                int sumSku = c.getInt(3);
                Date createDate = simpleDateFormat.parse(c.getString(4));
                SyncStatus syncStatus= SyncStatus.valueOf(c.getString(5));
                session=new Session(deviceNumber,sessionNumber,sumSku,createDate,syncStatus);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return session;
    }


    public int getLastSessionNumber() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        int lastSessionNumber = 0;
        String sql = "SELECT SESSION_NUMBER FROM SESSION ORDER BY SESSION_NUMBER DESC LIMIT 1";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            lastSessionNumber = c.getInt(1);
            c.moveToNext();
        }
        return lastSessionNumber;
    }

    public int getSumSkuBySession(String sessionNumber,String deviceNumber) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        int sumSku = 0;
        String sql = "SELECT * FROM SESSION WHERE SESSION.SESSION_NUMBER=? AND SESSION.DEVICE_NUMBER=?";
        Cursor c = db.rawQuery(sql, new String[]{String.valueOf(sessionNumber),deviceNumber});
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                sumSku = c.getInt(3);
                c.moveToNext();
            }
        }
        return sumSku;
    }

    public Boolean checkDuplicateSession(String sessionNumber,String deviceNumber) {
        Boolean flag = false;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "SELECT * FROM SESSION WHERE SESSION_NUMBER=? AND DEVICE_NUMBER=?";
        Cursor c = db.rawQuery(sql, new String[]{String.valueOf(sessionNumber),deviceNumber});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            if (c != null && c.getCount() > 0) {
                flag = true;
            }
            c.moveToNext();
        }
        return flag;
    }

    public Boolean isSessionIsDelete(String sessionNumber,String deviceNumber){
        Boolean flag = false;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "SELECT * FROM SESSION WHERE SESSION_NUMBER=? AND DEVICE_NUMBER=?";
        Cursor c = db.rawQuery(sql, new String[]{String.valueOf(sessionNumber),deviceNumber});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            if (c != null && c.getCount()<0) {
                flag = true;
            }
            c.moveToNext();
        }
        return flag;
    }


    public long insert(Session session) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("SESSION_NUMBER", session.getSessionNumber());
        values.put("DEVICE_NUMBER", session.getDeviceNumber());
        values.put("SUM_COIL_NO", session.getSumSku());
        values.put("CREATE_DATE",simpleDateFormat.format(Calendar.getInstance().getTime()));
        values.put("SYNC_STATUS", String.valueOf(SyncStatus.NOTSYNC));
        return db.insert("SESSION", null, values);
    }

    public int update(Session session) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("SESSION_NUMBER", session.getSessionNumber());
        values.put("DEVICE_NUMBER", session.getDeviceNumber());
        values.put("SUM_COIL_NO", session.getSumSku());
        values.put("CREATE_DATE",simpleDateFormat.format(session.getCreateDate()));
        values.put("SYNC_STATUS", String.valueOf(session.getSyncStatus()));
        return db.update("SESSION", values, "SESSION_NUMBER=? AND DEVICE_NUMBER=?", new String[]{String.valueOf(session.getSessionNumber()),session.getDeviceNumber()});
    }

    public int delete(Session session) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("SESSION", "SESSION_NUMBER=? AND DEVICE_NUMBER=?", new String[]{String.valueOf(session.getSessionNumber()),session.getDeviceNumber()});
    }

    public int delete(int idSession) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("SESSION", "ID_SESSION=?", new String[]{String.valueOf(idSession)});
    }

    public int deleteByDevice(Session session) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("SESSION", "DEVICE_NUMBER=?", new String[]{session.getDeviceNumber()});
    }

    public int deleteAll() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete("SESSION", null, null);
    }

    public ArrayList<Session> getSessionByMonth(String month){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<Session> list = new ArrayList<>();
        String sql = "SELECT * FROM SESSION WHERE STRFTIME('%m',SESSION.CREATE_DATE)=?";
        Cursor c = db.rawQuery(sql, new String[]{month});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                String sessionNumber = c.getString(1);
                String deviceNumber = c.getString(2);
                int sumSku = c.getInt(3);
                Date createDate = simpleDateFormat.parse(c.getString(4));
                SyncStatus syncStatus= SyncStatus.valueOf(c.getString(5));
                list.add(new Session(deviceNumber, sessionNumber, sumSku,createDate,syncStatus));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<Session> getSessionByYear(int year){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<Session> list = new ArrayList<>();
        String sql = "SELECT * FROM SESSION WHERE STRFTIME('%Y',SESSION.CREATE_DATE)=?";
        Cursor c = db.rawQuery(sql, new String[]{String.valueOf(year)});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                String sessionNumber = c.getString(1);
                String deviceNumber = c.getString(2);
                int sumSku = c.getInt(3);
                Date createDate = simpleDateFormat.parse(c.getString(4));
                SyncStatus syncStatus= SyncStatus.valueOf(c.getString(5));
                list.add(new Session(deviceNumber, sessionNumber, sumSku,createDate,syncStatus));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<Session> getSessionByYearMonth(String year,String month){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<Session> list = new ArrayList<>();
        String sql = "SELECT * FROM SESSION WHERE STRFTIME('%Y-%m',SESSION.CREATE_DATE)=?";
        Cursor c = db.rawQuery(sql, new String[]{year+"-"+month});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                String sessionNumber = c.getString(1);
                String deviceNumber = c.getString(2);
                int sumSku = c.getInt(3);
                Date createDate = simpleDateFormat.parse(c.getString(4));
                SyncStatus syncStatus= SyncStatus.valueOf(c.getString(5));
                list.add(new Session(deviceNumber, sessionNumber, sumSku,createDate,syncStatus));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<String> getListDevice(){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<String> list = new ArrayList<>();
        String sql = "SELECT * FROM SESSION";
        Cursor c = db.rawQuery(sql, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
                String deviceNumber = c.getString(2);
                list.add(deviceNumber);
            c.moveToNext();
        }
        return list;
    }

    public ArrayList<Session> getSessionByDevice(String deviceNumber1){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        ArrayList<Session> list = new ArrayList<>();
        String sql = "SELECT * FROM SESSION WHERE DEVICE_NUMBER=?";
        Cursor c = db.rawQuery(sql, new String[]{deviceNumber1});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            try {
                String sessionNumber = c.getString(1);
                String deviceNumber = c.getString(2);
                int sumSku = c.getInt(3);
                Date createDate = simpleDateFormat.parse(c.getString(4));
                SyncStatus syncStatus= SyncStatus.valueOf(c.getString(5));
                list.add(new Session(deviceNumber, sessionNumber, sumSku,createDate,syncStatus));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.moveToNext();
        }
        return list;
    }

    public int getId(String sessionNumber,String deviceNumber){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        int id=0;
        String sql = "SELECT * FROM SESSION WHERE SESSION_NUMBER=? AND DEVICE_NUMBER=?";
        Cursor c = db.rawQuery(sql, new String[]{String.valueOf(sessionNumber),deviceNumber});
        c.moveToFirst();
        while (!c.isAfterLast()) {
            id=c.getInt(0);
            c.moveToNext();
        }
        return id;
    }
}
