package com.symbol.barcodesample1.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alexandrius.accordionswipelayout.library.SwipeLayout;
import com.symbol.barcodesample1.R;
import com.symbol.barcodesample1.Model.SessionDetail;

import java.util.ArrayList;

public class SessionDetailAdapter2 extends RecyclerView.Adapter<SessionDetailAdapter2.ViewHolder> implements Filterable {
    Context context;
    ArrayList<SessionDetail> list;
    private ArrayList<SessionDetail> origsessionDetails;
    SessionDetail sessionDetail;
    ItemClickListener listener;
    ItemSwipeListener itemSwipeListener;
    private SessionDetailFilter sessionDetailFilter;

    public SessionDetailAdapter2(Context context, ArrayList<SessionDetail> list, ItemClickListener listener,ItemSwipeListener itemSwipeListener) {
        this.context = context;
        this.list = list;
        this.itemSwipeListener=itemSwipeListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.swipe_layout, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new ViewHolder(v);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        sessionDetail=list.get(i);
        viewHolder.itemCoilNo.setText(sessionDetail.getCoilNo());
        viewHolder.itemInspector.setText(sessionDetail.getInspector());
        for(int j=0;j<=i;j++){
            viewHolder.itemNo.setText(j+1+"");
        }

        viewHolder.rlSessionDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onClick(list.get(i));
                }
            }
        });

        viewHolder.swipeLayout.setOnSwipeItemClickListener(new SwipeLayout.OnSwipeItemClickListener() {
            @Override
            public void onSwipeItemClick(boolean left, int index) {
                if (itemSwipeListener != null) {
                    itemSwipeListener.onSwipe(list.get(i),left,index);
                }
            }
        });

        if(i%2==0){
            viewHolder.rlSessionDetail.setBackgroundColor(Color.parseColor("#CDE0F1"));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public Filter getFilter() {
        if(sessionDetailFilter==null){
            sessionDetailFilter=new SessionDetailFilter();
        }
        return sessionDetailFilter;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView itemCoilNo;
        TextView itemInspector;
        TextView itemNo;
        RelativeLayout rlSessionDetail;
        SwipeLayout swipeLayout;
        public ViewHolder(final View itemView) {
            super(itemView);
            itemCoilNo = (TextView)itemView.findViewById(R.id.tvCoilNo);
            itemInspector = (TextView) itemView.findViewById(R.id.tvInspector);
            itemNo=(TextView)itemView.findViewById(R.id.tvNo);
            rlSessionDetail=itemView.findViewById(R.id.rlSessionDetail);
            swipeLayout=itemView.findViewById(R.id.swipe_layout);
        }
    }

    public interface ItemClickListener {
        void onClick(SessionDetail sessionDetail);
    }

    public interface ItemSwipeListener{
        void onSwipe(SessionDetail sessionDetail,boolean left,int index);
    }

    private class SessionDetailFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            FilterResults results = new FilterResults();
            // We implement here the filter logic
            if (charSequence == null || charSequence.length() == 0) {
                // No filter implemented we return all the list
                results.values = origsessionDetails;
                results.count = origsessionDetails.size();
            }
            else {
                // We perform filtering operation
                ArrayList<SessionDetail> sessionDetails = new ArrayList<SessionDetail>();

                for (SessionDetail sessionDetail : sessionDetails) {
                    if (sessionDetail.getCoilNo().toUpperCase().startsWith(charSequence.toString().toUpperCase()))
                        sessionDetails.add(sessionDetail);
                }
                results.values = list;
                results.count = list.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            // Now we have to inform the adapter about the new list filtered
            list= (ArrayList<SessionDetail>) filterResults.values;
            notifyDataSetChanged();
        }
    }

}
