package com.symbol.barcodesample1.Util;

import com.symbol.barcodesample1.Model.ExportBook;

import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;

public class ExcelWriter2 {

    private static String produceCsvData(ExportBook[] data) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        if (data.length == 0) {
            return "";
        }

        Class classType = data[0].getClass();
        StringBuilder builder = new StringBuilder();

        Method[] methods = classType.getDeclaredMethods();

        builder.append("Coil No.,Size,Net Weight,Gross Weight,Length,Quality Standard,Classification,Coating Mass,Property,Surface Treatment,Date Of Product,Inspector");
        builder.append('\n');

//        for (Object d : data) {
//            for (Method m : methods) {
//                if (m.getParameterTypes().length == 0) {
//                    if (m.getName().startsWith("get") || m.getName().startsWith("is")) {
//                        System.out.println(m.invoke(d).toString());
//                        builder.append(m.invoke(d).toString()).append(",");
//                    }
//                }
//            }
//            builder.append('\n');
//        }
//        builder.deleteCharAt(builder.length() - 1);
        for(ExportBook exportBook:data){
            builder.append(exportBook.getCoilNo()).append(",");
            builder.append(exportBook.getSize()).append(",");
            builder.append(exportBook.getNetWeight()).append(",");
            builder.append(exportBook.getGrossWeight()).append(",");
            builder.append(exportBook.getLength()).append(",");
            builder.append(exportBook.getQualityStandard()).append(",");
            builder.append(exportBook.getClassification()).append(",");
            builder.append(exportBook.getCoatingMass()).append(",");
            builder.append(exportBook.getProperty()).append(",");
            builder.append(exportBook.getSurfaceTreatment()).append(",");
            builder.append(exportBook.getDateOfProduction()).append(",");
            builder.append(simpleDateFormat.format(exportBook.getInspector()));
            builder.append('\n');
        }

        return builder.toString();
    }

    public static boolean generateCSV(File csvFileName, ExportBook[] data) {
        FileWriter fw = null;
        try {
            fw = new FileWriter(csvFileName);
            if (!csvFileName.exists())
                csvFileName.createNewFile();
            fw.write(produceCsvData(data));
            fw.flush();
        } catch (Exception e) {
            System.out.println("Error while generating csv from data. Error message : " + e.getMessage());
            e.printStackTrace();
            return false;
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (Exception e) {
                }
                fw = null;
            }
        }
        return true;
    }
}
