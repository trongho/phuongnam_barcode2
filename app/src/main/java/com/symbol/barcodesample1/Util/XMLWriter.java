package com.symbol.barcodesample1.Util;

import android.os.Environment;
import android.util.Xml;
import android.widget.Toast;

import com.symbol.barcodesample1.Model.ExportBook;
import com.symbol.barcodesample1.Model.SessionDetail;

import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class XMLWriter {


    static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static boolean productXMLData(File xmlFileName,ArrayList<SessionDetail> data) {
        try {
            final String xml;
            XmlSerializer serializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            serializer.setOutput(writer);
            serializer.startDocument("UTF-8", true);
            serializer.startTag("", "LABEL");
            serializer.startTag("", "HEADER");

            serializer.startTag("", "BRAND");
            serializer.text("Tôn Hoa Sen");
            serializer.endTag("", "BRAND");

            serializer.startTag("", "PACKAGE");
            serializer.text("Thép dày mạ kẽm dạng cuộn");
            serializer.endTag("", "PACKAGE");

            serializer.startTag("", "LOCATION");
            serializer.text("Nhà máy tôn Hoa Sen Phú Mỹ");
            serializer.endTag("", "LOCATION");

            for (SessionDetail sessionDetail : data) {
                serializer.startTag("", "DETAILS");

                serializer.startTag("", "COILNO");
                serializer.text(sessionDetail.getCoilNo());
                serializer.endTag("", "COILNO");

                serializer.startTag("", "SIZE");
                serializer.text(String.valueOf(sessionDetail.getSize()));
                serializer.endTag("", "SIZE");

                serializer.startTag("", "NETWEIGHT");
                serializer.text(String.valueOf(sessionDetail.getNetWeight()));
                serializer.endTag("", "NETWEIGHT");

                serializer.startTag("", "GROSSWEIGHT");
                serializer.text(String.valueOf(sessionDetail.getGrossWeight()));
                serializer.endTag("", "GROSSWEIGHT");

                serializer.startTag("", "LENGTH");
                serializer.text(String.valueOf(sessionDetail.getLength()));
                serializer.endTag("", "LENGTH");

                serializer.startTag("", "QUALITYSTANDARD");
                serializer.text(sessionDetail.getQualityStandard());
                serializer.endTag("", "QUALITYSTANDARD");

                serializer.startTag("", "CLASSIFICATION");
                serializer.text(sessionDetail.getClassification());
                serializer.endTag("", "CLASSIFICATION");

                serializer.startTag("", "COATINGMASS");
                serializer.text(sessionDetail.getCoatingMass());
                serializer.endTag("", "COATINGMASS");

                serializer.startTag("", "PROPERTY");
                serializer.text(sessionDetail.getProperty());
                serializer.endTag("", "PROPERTY");

                serializer.startTag("", "SURFACETREATMENT");
                serializer.text(sessionDetail.getSurfaceTreatment());
                serializer.endTag("", "SURFACETREATMENT");

                serializer.startTag("", "DATEOFPRODUCT");
                serializer.text(simpleDateFormat.format(sessionDetail.getDateOfProduction()));
                serializer.endTag("", "DATEOFPRODUCT");

                serializer.startTag("", "INSPECTOR");
                serializer.text(sessionDetail.getInspector());
                serializer.endTag("", "INSPECTOR");

                serializer.endTag("", "DETAILS");
            }

            serializer.endTag("", "HEADER");
            serializer.endTag("", "LABEL");
            serializer.endDocument();
            xml = writer.toString();
            System.out.println(xml);

            FileWriter fw = null;
            try {
                fw = new FileWriter(xmlFileName);
                if (!xmlFileName.exists()) {
                    xmlFileName.createNewFile();
                }

                fw.write(xml);
                fw.flush();
            } catch (Exception e) {
                System.out.println("Error while generating xml from data. Error message : " + e.getMessage());
                e.printStackTrace();
                return false;
            } finally {
                if (fw != null) {
                    try {
                        fw.close();
                    } catch (Exception e) {
                    }
                    fw = null;
                }
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return  true;
    }
}


