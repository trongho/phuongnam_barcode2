package com.symbol.barcodesample1.Util;

import android.os.Environment;
import android.util.Log;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ConnectServer {
    static String result="";
    public static String connect(final String ip) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                FTPClient client = new FTPClient();
                try {
                    client.connect(ip);
                    // When login success the login method returns true.
                    boolean login = client.login("admin", "123456");
                    if (login) {
                        result="success to connect to server "+ip;
                        // When logout success the logout method returns true.
//                        boolean logout = client.logout();
//                        if (logout) {
//                            System.out.println("Logout from FTP server...");
//                        }
                    } else {
                        result="fail to connect to server "+ip;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        // Closes the connection to the FTP server
                        client.disconnect();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.start();
        return result;
    }

    public static void sendFileToPC(final String ip, final String fileName) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    FTPClient con = null;
                    con = new FTPClient();
                    con.connect(ip);

                    if (con.login("admin", "123456")) {
                        con.enterLocalPassiveMode(); // important!
                        con.setFileType(FTP.BINARY_FILE_TYPE);

                        FileInputStream in = new FileInputStream(new File(Environment.getExternalStorageDirectory(), fileName));
                        System.out.println(fileName + "");
                        boolean result = con.storeFile(fileName, in);
                        in.close();
                        if (result) {
                            Log.d("upload result", "succeeded");
                            System.out.println("send to pc success");
                        }
                        con.logout();
                        con.disconnect();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }
}
