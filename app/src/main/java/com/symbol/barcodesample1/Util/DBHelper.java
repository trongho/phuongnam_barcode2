package com.symbol.barcodesample1.Util;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

public class DBHelper extends SQLiteOpenHelper {
    public final static String DBNAME = "BARCODE_MANAGER";
    public final static int DBVERSION = 1;

    public DBHelper(Context context) {
        super(context, DBNAME, null, DBVERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sql;
        //creat session table
        sql = "CREATE TABLE SESSION(" +
                "ID_SESSION INTEGER PRIMARY KEY AUTOINCREMENT," +
                "SESSION_NUMBER INTEGER NOT NULL," +
                "DEVICE_NUMBER TEXT NOT NULL,"+
                "SUM_COIL_NO INTEGER NOT NULL,"+
                "CREATE_DATE DATE NOT NULL,"+
                "SYNC_STATUS TEXT NOT NULL)";
        sqLiteDatabase.execSQL(sql);

        //creat session detail table
        sql = "CREATE TABLE SESSION_DETAIL(" +
                "ID_SESSION_DETAIL INTEGER PRIMARY KEY AUTOINCREMENT," +
                "ID_SESSION INTEGER NOT NULL REFERENCES SESSION(ID_SESSION)," +
                "COIL_NO TEXT NOT NULL,"+
                "SIZE REAL NOT NULL,"+
                "NET_WEIGHT REAL NOT NULL,"+
                "GROSS_WEIGHT REAl,"+
                "LENGTH REAL NOT NULL,"+
                "QUALITY_STANDARD TEXT NOT NULL,"+
                "CLASSIFICATION TEXT NOT NULL,"+
                "COATING_MASS TEXT NOT NULL,"+
                "PROPERTY TEXT NOT NULL,"+
                "SURFACE_TREATMENT TEXT NOT NULL,"+
                "DATE_OF_PRODUCTION DATE NOT NULL,"+
                "INSPECTOR TEXT NOT NULL)";
        sqLiteDatabase.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String sql;
        sql = "DROP TABLE IF EXISTS SESSION";
        sqLiteDatabase.execSQL(sql);
        sql = "DROP TABLE IF EXISTS SESSION_DETAIL";
        sqLiteDatabase.execSQL(sql);
        onCreate(sqLiteDatabase);
    }
}
