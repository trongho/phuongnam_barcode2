package com.symbol.barcodesample1.UI;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.symbol.barcodesample1.R;

public class SettingActivity extends AppCompatActivity {
//    private CheckBox chkInverse1mode=null;
    private CheckBox chkContinous=null;
    private CheckBox chkSyncAndDelete=null;
    private Button btnSave=null;
    private Button btnCancel=null;
    private TextView tvSaved=null;

//    private Boolean inverse1Mode=false;
    private Boolean continousMode=false;
    private Boolean syncAndDelete=false;
//    public static  final String INVERSE1MODE="inverser 1 mode";
    public static final String CONTINOUS_MODE="continous mode";
    public static final String SYNC_AND_DELETE="sync and delete";
    private SharedPreferences storage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        setTitle("Setting");
        initComponent();
        setCheckedCheckbox();

//        chkInverse1mode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean ischecked) {
//                tvSaved.setText("");
//                if(ischecked==true){
//                    inverse1Mode=true;
//                }
//                else {
//                    inverse1Mode=false;
//                }
//            }
//        });

        chkContinous.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean ischecked) {
                tvSaved.setText("");
                if(ischecked==true){
                    continousMode=true;
                }
                else {
                    continousMode=false;
                }
            }
        });

        chkSyncAndDelete.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean ischecked) {
                tvSaved.setText("");
                if(ischecked==true){
                    syncAndDelete=true;
                }
                else {
                    syncAndDelete=false;
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                inverser1ModeSetting();
                continousSetting();
                syncAndSaveSetting();
                tvSaved.setText("Saved");
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initComponent(){
//        chkInverse1mode=findViewById(R.id.chkInverse1Mode);
        chkContinous=findViewById(R.id.chkContinous);
        chkSyncAndDelete=findViewById(R.id.chkSysnAndDelete);
        btnSave=findViewById(R.id.btnSaveSetting);
        btnCancel=findViewById(R.id.btnCancel);
        tvSaved=findViewById(R.id.tvSavedSetting);
        storage = getSharedPreferences(LoginActivity.PREFERENCES, Context.MODE_PRIVATE);
    }

//    private void inverser1ModeSetting(){
//        storage = getSharedPreferences(LoginActivity.PREFERENCES, Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = storage.edit();
//        editor.putBoolean(INVERSE1MODE,inverse1Mode);
//        editor.commit();
//    }

    private void continousSetting(){
        storage = getSharedPreferences(LoginActivity.PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = storage.edit();
        editor.putBoolean(CONTINOUS_MODE,continousMode);
        editor.commit();
    }

    private void syncAndSaveSetting(){
        storage = getSharedPreferences(LoginActivity.PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = storage.edit();
        editor.putBoolean(SYNC_AND_DELETE,syncAndDelete);
        editor.commit();
    }

    private void setCheckedCheckbox(){
//        if (String.valueOf(storage.getBoolean(SettingActivity.INVERSE1MODE,false))!=null) {
//            inverse1Mode=storage.getBoolean(SettingActivity.INVERSE1MODE,false);
//            chkInverse1mode.setChecked(inverse1Mode);
//        }
        if (String.valueOf(storage.getBoolean(SettingActivity.CONTINOUS_MODE,false))!=null) {
            continousMode=storage.getBoolean(SettingActivity.CONTINOUS_MODE,false);
            chkContinous.setChecked(continousMode);
        }
        if (String.valueOf(storage.getBoolean(SettingActivity.SYNC_AND_DELETE,false))!=null) {
            syncAndDelete=storage.getBoolean(SettingActivity.SYNC_AND_DELETE,false);
            chkSyncAndDelete.setChecked(syncAndDelete);
        }
    }
}