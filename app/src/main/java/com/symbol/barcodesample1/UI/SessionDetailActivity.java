package com.symbol.barcodesample1.UI;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.symbol.barcodesample1.R;
import com.symbol.barcodesample1.Model.Session;
import com.symbol.barcodesample1.DAO.SessionDAO;
import com.symbol.barcodesample1.Model.SessionDetail;
import com.symbol.barcodesample1.DAO.SessionDetailDAO;
import com.symbol.barcodesample1.Model.SyncStatus;
import com.symbol.barcodesample1.Adapter.SessionDetailAdapter2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class SessionDetailActivity extends AppCompatActivity {
    private RecyclerView rvSessionDetail = null;
    private Toolbar toolbar = null;
    String sessionNumber;
    String deviceNumber;
    SyncStatus syncStatus;
    Date createDate;
    int idSession;
    String creatDate;
    TextView tvNoData;
    private Button btnDeleteAllBook = null;
    private Button btnContinueScan = null;
    private int sortIndex = 0;
    SessionDetail sessionDetail;
    SessionDetailDAO sessionDetailDAO;
    SessionDAO sessionDAO;
    ArrayList<SessionDetail> sessionDetailArrayList;
    SessionDetailAdapter2 sessionDetailAdapter2;
    public static final String CREATE_DATE = "create date";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_detail);
        initComponent();
        getIntentData();
        getIdSession();
        setToolbar();
        updateLv();

        btnDeleteAllBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionDetailDAO.delete(idSession);
                Session session = null;
                try {
                    session = new Session(deviceNumber, sessionNumber, 0
                            , simpleDateFormat.parse(creatDate), syncStatus);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                sessionDAO.update(session);
                Toast.makeText(SessionDetailActivity.this
                        , "Success delete data session " + sessionNumber, Toast.LENGTH_SHORT).show();
                updateLv();
            }
        });

        btnContinueScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SessionDetailActivity.this, MainActivity3.class);
                intent.putExtra(MainActivity3.SESSION_NUMBER, sessionNumber);
                intent.putExtra(MainActivity3.DEVICE_NUMBER, deviceNumber);
                intent.putExtra(SessionDetailActivity.CREATE_DATE, creatDate);
                intent.putExtra(MainActivity3.SYNC_STATUS, syncStatus + "");
                startActivity(intent);
            }
        });


//        setSortSpinner();
//        addSpSortListener();
    }

    public void initComponent() {
        tvNoData = findViewById(R.id.tvNoData);
        btnDeleteAllBook = findViewById(R.id.btnDeleteAllBook);
        btnContinueScan = findViewById(R.id.btnContinueScan);
        rvSessionDetail = findViewById(R.id.rvSessionDetail);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        sessionDetailDAO = new SessionDetailDAO(SessionDetailActivity.this);
        sessionDAO = new SessionDAO(SessionDetailActivity.this);
    }

    public void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        String tittle = "MÁY " + deviceNumber + "_" + "PHIÊN " + sessionNumber;
        getSupportActionBar().setTitle(tittle);
        toolbar.setTitleTextColor(Color.parseColor("#2C5CA4"));
    }

    public void getIdSession() {
        idSession = sessionDAO.getId(sessionNumber, deviceNumber);
    }

    public void getIntentData() {
        Intent intent = getIntent();
        sessionNumber = intent.getStringExtra(MainActivity3.SESSION_NUMBER);
        deviceNumber = intent.getStringExtra(MainActivity3.DEVICE_NUMBER);
        try {
            createDate = simpleDateFormat.parse(intent.getStringExtra(SessionDetailActivity.CREATE_DATE));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        syncStatus = SyncStatus.valueOf(intent.getStringExtra(MainActivity3.SYNC_STATUS));
        creatDate = intent.getStringExtra(SessionDetailActivity.CREATE_DATE);
    }

    public void setAdapter() {
        sessionDetailAdapter2 = new SessionDetailAdapter2(SessionDetailActivity.this, sessionDetailArrayList, new SessionDetailAdapter2.ItemClickListener() {
            @Override
            public void onClick(SessionDetail sessionDetail) {

            }
        }, new SessionDetailAdapter2.ItemSwipeListener() {
            @Override
            public void onSwipe(SessionDetail sessionDetail, boolean left, int index) {
                if (index == 1) {
                    deleteSessionDetailDialog(sessionDetail);
                }
            }
        });
        rvSessionDetail.setLayoutManager(new LinearLayoutManager(SessionDetailActivity.this));
        rvSessionDetail.setAdapter(sessionDetailAdapter2);
        if (sessionDetailArrayList.size() == 0) {
            tvNoData.setText("Chưa có dữ liệu cho phiên này");
        }
    }

    public void deleteSessionDetailDialog(final SessionDetail sessionDetail1) {
        AlertDialog.Builder builder = new AlertDialog.Builder(SessionDetailActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_delete_session_detail, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                updateLv();
            }
        });

        Button btnYes, btnNo;
        btnYes = alertDialog.findViewById(R.id.btnYes);
        btnNo = alertDialog.findViewById(R.id.btnNo);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int result = sessionDetailDAO.deleteByID(String.valueOf(sessionDetail1.getId()));
                if (result > 0) {
                    alertDialog.cancel();
                    sessionDAO.update(new Session(deviceNumber, sessionNumber
                            , sessionDAO.getSumSkuBySession(sessionNumber, deviceNumber) - 1, createDate, syncStatus));
                    Toast.makeText(SessionDetailActivity.this, "Xóa thành công mã " + sessionDetail1.getCoilNo(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    public void editQuantityDialog(final SessionDetail sessionDetail1) {
        AlertDialog.Builder builder = new AlertDialog.Builder(SessionDetailActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_edit_session_detail, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                updateLv();
            }
        });

        final EditText edtCoilNo = alertDialog.findViewById(R.id.edtCoilNo);
        final EditText edtSize = alertDialog.findViewById(R.id.edtSize);
        final EditText edtNetWeight = alertDialog.findViewById(R.id.edtNetWeight);
        final EditText edtGrossWeight = alertDialog.findViewById(R.id.edtGrossWeight);
        final EditText edtLength = alertDialog.findViewById(R.id.edtLength);
        final EditText edtQualityStandard = alertDialog.findViewById(R.id.edtQualityStandard);
        final EditText edtClassification = alertDialog.findViewById(R.id.edtClassification);
        final EditText edtCoatingMass = alertDialog.findViewById(R.id.edtCoatingMass);
        final EditText edtProperty = alertDialog.findViewById(R.id.edtProperty);
        final EditText edtSurfaceTreatment = alertDialog.findViewById(R.id.edtSurfaceTreatment);
        final EditText edtDateOfProduction = alertDialog.findViewById(R.id.edtDateOfProduction);
        final EditText edtInspector = alertDialog.findViewById(R.id.edtInspector);
        Button btnSave = alertDialog.findViewById(R.id.btnSave);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);

        edtCoilNo.setText(sessionDetail1.getCoilNo());
        edtSize.setText(sessionDetail1.getSize() + "");
        edtNetWeight.setText(sessionDetail1.getNetWeight() + "");
        edtGrossWeight.setText(sessionDetail1.getGrossWeight() + "");
        edtLength.setText(sessionDetail1.getLength() + "");
        edtQualityStandard.setText(sessionDetail1.getQualityStandard());
        edtClassification.setText(sessionDetail1.getClassification());
        edtCoatingMass.setText(sessionDetail1.getCoatingMass());
        edtProperty.setText(sessionDetail1.getProperty());
        edtSurfaceTreatment.setText(sessionDetail1.getSurfaceTreatment());
        edtDateOfProduction.setText(simpleDateFormat.format(sessionDetail1.getDateOfProduction()));
        edtInspector.setText(sessionDetail1.getInspector());
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(edtCoilNo.getText().toString())) {
                    Toast.makeText(SessionDetailActivity.this, "Mã số cuộn không được để trống", Toast.LENGTH_SHORT).show();
                    edtCoilNo.requestFocus();
                    return;
                }
                try {
                    sessionDetail1.setId(sessionDetail1.getId());
                    sessionDetail1.setCoilNo(edtCoilNo.getText().toString());
                    sessionDetail1.setSize(Double.parseDouble(edtSize.getText().toString()));
                    sessionDetail1.setNetWeight(Double.parseDouble(edtNetWeight.getText().toString()));
                    sessionDetail1.setGrossWeight(Double.parseDouble(edtGrossWeight.getText().toString()));
                    sessionDetail1.setLength(Double.parseDouble(edtLength.getText().toString()));
                    sessionDetail1.setQualityStandard(edtQualityStandard.getText().toString());
                    sessionDetail1.setClassification(edtClassification.getText().toString());
                    sessionDetail1.setCoatingMass(edtCoatingMass.getText().toString());
                    sessionDetail1.setProperty(edtProperty.getText().toString());
                    sessionDetail1.setSurfaceTreatment(edtSurfaceTreatment.getText().toString());
                    sessionDetail1.setInspector(edtInspector.getText().toString());
                    sessionDetail1.setDateOfProduction(simpleDateFormat.parse(edtDateOfProduction.getText().toString()));

                    int result = sessionDetailDAO.updateByID(String.valueOf(sessionDetail1.getId()), sessionDetail1);
                    if (result > 0) {
                        sessionDAO.update(new Session(deviceNumber, sessionNumber
                                , sessionDAO.getSumSkuBySession(sessionNumber, deviceNumber), createDate, syncStatus));
                        alertDialog.cancel();
                        View content = findViewById(android.R.id.content);
                        Snackbar.make(content, "Success edit", Snackbar.LENGTH_SHORT).show();
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    public void updateLv() {
        sessionDetailArrayList = sessionDetailDAO.getBySession(idSession);
        setAdapter();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.session_detail_menu, menu);
        final MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setQueryHint("Search coil no.");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                ArrayList<SessionDetail> searchlist = new ArrayList<>();
                if (s.length() == 0) {
                    searchlist = sessionDetailArrayList;
                } else {
                    for (SessionDetail sessionDetail : sessionDetailArrayList) {
                        if (sessionDetail.getCoilNo().toLowerCase().contains(s.toString().toLowerCase())) {
                            searchlist.add(sessionDetail);
                        }
                    }
                }
                sessionDetailAdapter2 = new SessionDetailAdapter2(SessionDetailActivity.this, searchlist, new SessionDetailAdapter2.ItemClickListener() {
                    @Override
                    public void onClick(SessionDetail sessionDetail) {

                    }
                }, new SessionDetailAdapter2.ItemSwipeListener() {
                    @Override
                    public void onSwipe(SessionDetail sessionDetail, boolean left, int index) {
                       if (index == 0) {
                            deleteSessionDetailDialog(sessionDetail);
                        }
                    }
                });
                rvSessionDetail.setLayoutManager(new LinearLayoutManager(SessionDetailActivity.this));
                rvSessionDetail.setAdapter(sessionDetailAdapter2);
                if (sessionDetailArrayList.size() == 0) {
                    tvNoData.setText("Chưa có dữ liệu cho phiên này");
                }
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        if (id == R.id.action_search) {

        }
        return super.onOptionsItemSelected(item);
    }

}