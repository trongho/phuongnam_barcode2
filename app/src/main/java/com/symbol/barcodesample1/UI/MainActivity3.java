/*
 * Copyright (C) 2015-2019 Zebra Technologies Corporation and/or its affiliates
 * All rights reserved.
 */
package com.symbol.barcodesample1.UI;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.symbol.barcodesample1.Model.Book;
import com.symbol.barcodesample1.Model.ExportBook;
import com.symbol.barcodesample1.Util.ConnectServer;
import com.symbol.barcodesample1.R;
import com.symbol.barcodesample1.Model.Session;
import com.symbol.barcodesample1.DAO.SessionDAO;
import com.symbol.barcodesample1.Model.SessionDetail;
import com.symbol.barcodesample1.DAO.SessionDetailDAO;
import com.symbol.barcodesample1.Model.SyncStatus;
import com.symbol.barcodesample1.Adapter.SessionDetailAdapter2;
import com.symbol.barcodesample1.Util.ExcelWriter;
import com.symbol.barcodesample1.Util.ExcelWriter2;
import com.symbol.barcodesample1.Util.XMLWriter;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.EMDKManager.EMDKListener;
import com.symbol.emdk.EMDKManager.FEATURE_TYPE;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.BarcodeManager.ConnectionState;
import com.symbol.emdk.barcode.BarcodeManager.ScannerConnectionListener;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.ScannerConfig;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerInfo;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.ScanDataCollection.ScanData;
import com.symbol.emdk.barcode.Scanner.DataListener;
import com.symbol.emdk.barcode.Scanner.StatusListener;
import com.symbol.emdk.barcode.Scanner.TriggerType;
import com.symbol.emdk.barcode.StatusData.ScannerStates;
import com.symbol.emdk.barcode.StatusData;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity3 extends AppCompatActivity implements EMDKListener, DataListener, StatusListener, ScannerConnectionListener {

    private EMDKManager emdkManager = null;
    private BarcodeManager barcodeManager = null;
    private Scanner scanner = null;

    private TextView textViewStatus = null;
    private TextView tvSessionNumber = null;
    private Button btnStopScan = null;
    private TextView tvSumCoilNo = null;
    private EditText edtCoilNo = null;
    private EditText edtSize = null;
    private EditText edtNetWeight = null;
    private EditText edtGrossWeight = null;
    private EditText edtLength = null;
    private EditText edtQualityStandard = null;
    private EditText edtClassification = null;
    private EditText edtCoatingMass = null;
    private EditText edtProperty = null;
    private EditText edtSurfaceTreatment = null;
    private EditText edtDateOfProduction = null;
    private EditText edtInspector = null;
    private Button btnExportCSVAndSync = null;
    private Button btnExportXMLAndSync = null;
    private View vBottom = null;
    private Toolbar toolbar = null;
    private RecyclerView rvSessionDetail;
    private Button btnSave = null;

    private List<ScannerInfo> deviceList = null;

    private String statusString = "";

    private boolean bSoftTriggerSelected = false;
    private boolean bDecoderSettingsChanged = false;
    private boolean bExtScannerDisconnected = false;
    private boolean bContinuousMode = false;
    private final Object lock = new Object();

    public static final String SESSION_NUMBER = "session number";
    public static final String DEVICE_NUMBER = "device number";
    public static final String SYNC_STATUS = "sync status";
    public static final String ID_SESSION = "id session";
    String sessionNumber;
    String deviceNumber;
    int idSession;
    String ipAdress;
    Date createDate;
    SharedPreferences storage;

    ArrayList<Book> bookArrayList;
    int sumCoilNo = 0;
    SessionDetailDAO sessionDetailDAO;
    SessionDetail sessionDetail;
    ArrayList<SessionDetail> sessionDetailArrayList;
    SessionDetailAdapter2 sessionDetailAdapter2;
    SessionDAO sessionDAO;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    Boolean bSyncAnDelete = false;
    private SyncStatus syncStatus;
    //    Boolean bInverse1Mode = false;
    private Vector<AlertDialog> dialogs = new Vector<AlertDialog>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        initComponent();
        getIntentData();
        getIdSession();
        setToolbar();
        getSetting();


        EMDKResults results = EMDKManager.getEMDKManager(getApplicationContext(), this);
        if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS) {
            updateStatus("EMDKManager object request failed!");
            return;
        }

        btnStopScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopScan();
            }
        });

        updateLv();

        btnExportCSVAndSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bSyncAnDelete == false) {
                    exportCSVAndSysnFile();
                    setSyncStatus();
                } else {
                    exportCSVAndSysnFile();
                    setSyncStatus();
                    sessionDetailDAO.delete(idSession);
                    sessionDAO.delete(idSession);
                }
            }
        });

        btnExportXMLAndSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bSyncAnDelete == false) {
                    exportXMLAndSysnFile();
                    setSyncStatus();
                } else {
                    exportXMLAndSysnFile();
                    setSyncStatus();
                    sessionDetailDAO.delete(idSession);
                    sessionDAO.delete(idSession);
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(edtCoilNo.getText().toString())) {
                    Toast.makeText(MainActivity3.this, "Mã số cuộn không được để trống", Toast.LENGTH_SHORT).show();
                    edtCoilNo.requestFocus();
                    return;
                }


                try {
                    String coilNo = edtCoilNo.getText().toString();
                    double size = Double.parseDouble(edtSize.getText().toString());
                    double netWeight = Double.parseDouble(edtNetWeight.getText().toString());
                    double grossWeight = !edtGrossWeight.getText().toString().equalsIgnoreCase("")?Double.parseDouble(edtGrossWeight.getText().toString()):0;
                    double length = Double.parseDouble(edtLength.getText().toString());
                    String qualityStandard = edtQualityStandard.getText().toString();
                    String classification = edtClassification.getText().toString();
                    String coatingMass = edtCoatingMass.getText().toString();
                    String property = edtProperty.getText().toString();
                    String surfaceTreatment = edtSurfaceTreatment.getText().toString();
                    Date dateOfProduction = simpleDateFormat.parse(edtDateOfProduction.getText().toString());
                    String inspector = edtInspector.getText().toString();
                    sessionDetail = new SessionDetail(idSession, coilNo, size, netWeight, grossWeight, length, qualityStandard, classification, coatingMass, property, surfaceTreatment, dateOfProduction, inspector);
                    addSessionDetailNotDuplicate(sessionDetail);

                    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                    updateLv();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        setSumSku();
        setSyncStatus();
    }

    public void initComponent() {
        btnSave = findViewById(R.id.btnSave);
        rvSessionDetail = findViewById(R.id.rvSessionDetail);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        vBottom = findViewById(R.id.vBottom);
        textViewStatus = (TextView) findViewById(R.id.textViewStatus);
        tvSessionNumber = findViewById(R.id.tvSessionNumber);
        btnStopScan = findViewById(R.id.btnStopScan);
        tvSumCoilNo = findViewById(R.id.tvSumColNo);
        edtCoilNo = findViewById(R.id.edtCoilNo);
        edtSize = findViewById(R.id.edtSize);
        edtNetWeight = findViewById(R.id.edtNetWeight);
        edtGrossWeight = findViewById(R.id.edtGrossWeight);
        edtLength = findViewById(R.id.edtLength);
        edtQualityStandard = findViewById(R.id.edtQualityStandard);
        edtClassification = findViewById(R.id.edtClassification);
        edtCoatingMass = findViewById(R.id.edtCoatingMass);
        edtProperty = findViewById(R.id.edtProperty);
        edtSurfaceTreatment = findViewById(R.id.edtSurfaceTreatment);
        edtDateOfProduction=findViewById(R.id.edtDateOfProduction);
        edtInspector=findViewById(R.id.edtInspector);
        btnExportCSVAndSync = findViewById(R.id.btnExportCSVAndSysn);
        btnExportXMLAndSync = findViewById(R.id.btnExportXMLAndSysn);
        bookArrayList = new ArrayList<>();

        sessionDetailArrayList = new ArrayList<>();
        sessionDetailDAO = new SessionDetailDAO(MainActivity3.this);
        sessionDAO = new SessionDAO(MainActivity3.this);
        storage = getSharedPreferences(LoginActivity.PREFERENCES, Context.MODE_PRIVATE);
    }

    public void setSyncStatus() {
//        textViewStatus.setText(syncStatus + "");
//        if (sessionDAO.getSession(sessionNumber, deviceNumber).getSyncStatus() == SyncStatus.SYNCFAIL) {
//            textViewStatus.setTextColor(Color.parseColor("#FF1517"));
//        } else if (sessionDAO.getSession(sessionNumber, deviceNumber).getSyncStatus() == SyncStatus.NOTSYNC) {
//            textViewStatus.setTextColor(Color.parseColor("#000000"));
//        }
    }

    public void setSumSku() {
        sumCoilNo = sessionDAO.getSumSkuBySession(sessionNumber, deviceNumber);
        tvSumCoilNo.setText("Tổng số lượng đã scan: " + sumCoilNo);
        Log.d("sum", sumCoilNo + "");
    }

    public void getSetting() {
//        if (String.valueOf(storage.getBoolean(SettingActivity.INVERSE1MODE, false)) != null) {
//            bInverse1Mode = storage.getBoolean(SettingActivity.INVERSE1MODE, false);
//        }
        if (String.valueOf(storage.getBoolean(SettingActivity.CONTINOUS_MODE, false)) != null) {
            bContinuousMode = storage.getBoolean(SettingActivity.CONTINOUS_MODE, false);
        }
        if (String.valueOf(storage.getBoolean(SettingActivity.SYNC_AND_DELETE, false)) != null) {
            bSyncAnDelete = storage.getBoolean(SettingActivity.SYNC_AND_DELETE, false);
        }
    }

    public void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        String tittle = "MÁY " + deviceNumber + "_" + "PHIÊN " + sessionNumber;
        getSupportActionBar().setTitle(tittle);
        toolbar.setTitleTextColor(Color.parseColor("#2C5CA4"));
    }

    public void exportCSVAndSysnFile() {
        if (isStoragePermissionGranted()) {
            String fileName = deviceNumber + sessionNumber + ".csv";

            ArrayList<SessionDetail> sessionDetailArrayListSort = sessionDetailDAO.getBySession(idSession);
            ExportBook[] exportBookArrayList =new ExportBook[sessionDetailArrayListSort.size()];

            for (int i = 0; i < sessionDetailArrayListSort.size(); i++) {
                String coilNo =sessionDetailArrayListSort.get(i).getCoilNo();
                double size = sessionDetailArrayListSort.get(i).getSize();
                double netWeight = sessionDetailArrayListSort.get(i).getNetWeight();
                double grossWeight = sessionDetailArrayListSort.get(i).getGrossWeight();
                double length = sessionDetailArrayListSort.get(i).getLength();
                String qualityStandard = sessionDetailArrayListSort.get(i).getClassification();
                String classification = sessionDetailArrayListSort.get(i).getCoilNo();
                String coatingMass = sessionDetailArrayListSort.get(i).getCoatingMass();
                String property = sessionDetailArrayListSort.get(i).getProperty();
                String surfaceTreatment = sessionDetailArrayListSort.get(i).getSurfaceTreatment();
                Date dateOfProduction = sessionDetailArrayListSort.get(i).getDateOfProduction();
                String inspector = sessionDetailArrayListSort.get(i).getInspector();
                exportBookArrayList[i] = new ExportBook(coilNo, size, netWeight, grossWeight, length, qualityStandard, classification, coatingMass, property, surfaceTreatment, dateOfProduction, inspector);
            }

            Boolean writeToCSV = ExcelWriter
                    .generateCSV(new File(Environment.getExternalStorageDirectory()
                            , fileName), exportBookArrayList);
            if (writeToCSV == true) {
                Toast.makeText(MainActivity3.this, "Export to CSV Success !!!", Toast.LENGTH_SHORT).show();
            }
            if (storage.getString(LoginActivity.SERVER_IP, "") != null) {
                ipAdress = storage.getString(LoginActivity.SERVER_IP, "");
            }
            String result = ConnectServer.connect(ipAdress);
            if (result.equalsIgnoreCase("success to connect to server " + ipAdress)) {
                ConnectServer.sendFileToPC(ipAdress, fileName);
                syncStatus = SyncStatus.SYNCED;
                Session session = new Session();
                session = sessionDAO.getSession(sessionNumber, deviceNumber);
//                session.setSyncStatus(syncStatus);
                sessionDAO.update(session);
                onButtonShowPopupWindowClick(vBottom);

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                backToCreateSession();
            } else {
                onButtonShowPopupWindowClick2(vBottom);
                syncStatus = SyncStatus.SYNCFAIL;
                Session session = new Session();
                session = sessionDAO.getSession(sessionNumber, deviceNumber);
                session.setSyncStatus(syncStatus);
                sessionDAO.update(session);
            }
        }
    }

    public void exportXMLAndSysnFile() {
        if (isStoragePermissionGranted()) {
            String fileName = deviceNumber + sessionNumber + ".xml";

            ArrayList<SessionDetail> sessionDetailArrayListSort = sessionDetailDAO.getBySession(idSession);

            Boolean writeToXML = XMLWriter
                    .productXMLData(new File(Environment.getExternalStorageDirectory()
                            , fileName), sessionDetailArrayListSort);
            if (writeToXML == true) {
                Toast.makeText(MainActivity3.this, "Export to XML Success !!!", Toast.LENGTH_SHORT).show();
            }
            if (storage.getString(LoginActivity.SERVER_IP, "") != null) {
                ipAdress = storage.getString(LoginActivity.SERVER_IP, "");
            }
            String result = ConnectServer.connect(ipAdress);
            if (result.equalsIgnoreCase("success to connect to server " + ipAdress)) {
                ConnectServer.sendFileToPC(ipAdress, fileName);
                syncStatus = SyncStatus.SYNCED;
                Session session = new Session();
                session = sessionDAO.getSession(sessionNumber, deviceNumber);
                session.setSyncStatus(syncStatus);
                sessionDAO.update(session);
                onButtonShowPopupWindowClick(vBottom);

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                backToCreateSession();
            } else {
                onButtonShowPopupWindowClick2(vBottom);
                syncStatus = SyncStatus.SYNCFAIL;
                Session session = new Session();
                session = sessionDAO.getSession(sessionNumber, deviceNumber);
                session.setSyncStatus(syncStatus);
                sessionDAO.update(session);
            }
        }
    }

    public void onButtonShowPopupWindowClick2(View view) {

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.bottomsheet_fail_connect, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });

        Button btnConnect = popupView.findViewById(R.id.btnConnect);
        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setting();
            }
        });
    }

    public void setting() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity3.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_setting, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
            }
        });

        final EditText edtIpAdress = alertDialog.findViewById(R.id.edtIpAdress);
        Button btnConnectServer = alertDialog.findViewById(R.id.btnConnectServer);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        final TextView tvConnectionResult = alertDialog.findViewById(R.id.tvConnectionResult);

        if (storage.getString(LoginActivity.SERVER_IP, "") != null) {
            edtIpAdress.setText(storage.getString(LoginActivity.SERVER_IP, ""));
        }

        btnConnectServer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                storage = getSharedPreferences(LoginActivity.PREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = storage.edit();
                editor.putString(LoginActivity.SERVER_IP, edtIpAdress.getText().toString());
                editor.commit();

                String result = ConnectServer.connect(edtIpAdress.getText().toString());
                tvConnectionResult.setText(result);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    public void onButtonShowPopupWindowClick(View view) {

        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.bottomsheet_success_connect, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        }
    }

    public void getIntentData() {
        Intent intent = getIntent();
        sessionNumber = intent.getStringExtra(SESSION_NUMBER);
        deviceNumber = intent.getStringExtra(DEVICE_NUMBER);
        try {
            createDate = simpleDateFormat.parse(intent.getStringExtra(SessionDetailActivity.CREATE_DATE));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        syncStatus = SyncStatus.valueOf(intent.getStringExtra(MainActivity3.SYNC_STATUS));
    }

    public void getIdSession() {
        idSession = sessionDAO.getId(sessionNumber, deviceNumber);
    }

    @Override
    public void onOpened(EMDKManager emdkManager) {
//       textViewStatus.setText("Status: " + "EMDK open success!");
        this.emdkManager = emdkManager;
        // Acquire the barcode manager resources
        barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
        // Enumerate scanner devices
        enumerateScannerDevices();

        //
        initScanner();

        stopScan();

    }

    @Override
    protected void onResume() {
        super.onResume();
        // The application is in foreground
        if (emdkManager != null) {
            // Acquire the barcode manager resources
            initBarcodeManager();
            // Enumerate scanner devices
            enumerateScannerDevices();
            // Initialize scanner
            initScanner();
            stopScan();
            getSetting();

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // The application is in background
        // Release the barcode manager resources
        deInitScanner();
        deInitBarcodeManager();
    }

    @Override
    public void onClosed() {
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
//        updateStatus("EMDK closed unexpectedly! Please close and restart the application.");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Release all the resources
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
        }
    }

    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        if ((scanDataCollection != null) && (scanDataCollection.getResult() == ScannerResults.SUCCESS)) {
            ArrayList<ScanData> scanData = scanDataCollection.getScanData();
            for (ScanData data : scanData) {
                updateData(data.getData());
            }
        }
    }

    @Override
    public void onStatus(StatusData statusData) {
        ScannerStates state = statusData.getState();
        switch (state) {
            case IDLE:
//                statusString = statusData.getFriendlyName() + " is enabled and idle...";
//                updateStatus(statusString);
                if (bContinuousMode) {
                    try {
                        scanner.triggerType = TriggerType.SOFT_ALWAYS;
                        // An attempt to use the scanner continuously and rapidly (with a delay < 100 ms between scans)
                        // may cause the scanner to pause momentarily before resuming the scanning.
                        // Hence add some delay (>= 100ms) before submitting the next read.
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        scanner.read();
                    } catch (ScannerException e) {
                        //updateStatus(e.getMessage());
                    }
                }
                // set trigger type
                if (bSoftTriggerSelected) {
                    scanner.triggerType = TriggerType.SOFT_ONCE;
                    bSoftTriggerSelected = false;
                } else {
                    scanner.triggerType = TriggerType.HARD;
                }
                // set decoders
                if (bDecoderSettingsChanged) {
                    setDecoders();
                    bDecoderSettingsChanged = false;
                } else {
                    setDecoders();
                }
                // submit read
                if (!scanner.isReadPending() && !bExtScannerDisconnected) {
                    try {
                        scanner.read();
                    } catch (ScannerException e) {
//                        updateStatus(e.getMessage());
                    }
                }
                break;
            case WAITING:
                statusString = "Nhấn nút scan hoặc phím cứng trên thiết bị để scan...";
//                updateStatus(statusString);
                break;
            case SCANNING:
                statusString = "Đang scan...";
//                updateStatus(statusString);
                break;
            case DISABLED:
                statusString = statusData.getFriendlyName() + " is disabled.";
//                updateStatus(statusString);
                break;
            case ERROR:
                statusString = "An error has occurred.";
//                updateStatus(statusString);
                break;
            default:
                break;
        }
    }

    @Override
    public void onConnectionChange(ScannerInfo scannerInfo, ConnectionState connectionState) {
        String status;
        String scannerName = "";
        String statusExtScanner = connectionState.toString();
        String scannerNameExtScanner = scannerInfo.getFriendlyName();
        if (deviceList.size() != 0) {
            scannerName = deviceList.get(1).getFriendlyName();
        }
        if (scannerName.equalsIgnoreCase(scannerNameExtScanner)) {
            switch (connectionState) {
                case CONNECTED:
                    bSoftTriggerSelected = false;
                    synchronized (lock) {
                        initScanner();
                        setDecoders();
                        bExtScannerDisconnected = false;
                    }
                    break;
                case DISCONNECTED:
                    bExtScannerDisconnected = true;
                    synchronized (lock) {
                        deInitScanner();
                    }
                    break;
            }
            status = scannerNameExtScanner + ":" + statusExtScanner;
//            updateStatus(status);
        } else {
            bExtScannerDisconnected = false;
            status = statusString + " " + scannerNameExtScanner + ":" + statusExtScanner;
//            updateStatus(status);
        }
    }

    private void initScanner() {
        if (scanner == null) {
            if ((deviceList != null) && (deviceList.size() != 0)) {
                if (barcodeManager != null)
                    scanner = barcodeManager.getDevice(deviceList.get(0));
            } else {
//                updateStatus("Failed to get the specified scanner device! Please close and restart the application.");
                return;
            }
            if (scanner != null) {
                scanner.addDataListener(this);
                scanner.addStatusListener(this);
                try {
                    scanner.enable();
                } catch (ScannerException e) {
                    //updateStatus(e.getMessage());
                    deInitScanner();
                }
            } else {
//                updateStatus("Failed to initialize the scanner device.");
            }
        }
    }

    private void deInitScanner() {
        if (scanner != null) {
            try {
                scanner.disable();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.removeDataListener(this);
                scanner.removeStatusListener(this);
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }

            try {
                scanner.release();
            } catch (Exception e) {
//                updateStatus(e.getMessage());
            }
            scanner = null;
        }
    }

    private void initBarcodeManager() {
        barcodeManager = (BarcodeManager) emdkManager.getInstance(FEATURE_TYPE.BARCODE);
        // Add connection listener
        if (barcodeManager != null) {
            barcodeManager.addConnectionListener(this);
        }
    }

    private void deInitBarcodeManager() {
        if (emdkManager != null) {
            emdkManager.release(FEATURE_TYPE.BARCODE);
        }
    }


    private void enumerateScannerDevices() {
        if (barcodeManager != null) {
            deviceList = barcodeManager.getSupportedDevicesInfo();
            if ((deviceList != null) && (deviceList.size() != 0)) {
//                deviceList.remove(3);
            } else {
//                updateStatus("Failed to get the list of supported scanner devices! Please close and restart the application.");
            }
        }
    }

    private void setDecoders() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                // Set EAN8
                config.decoderParams.ean8.enabled = true;
                // Set EAN13
                config.decoderParams.ean13.enabled = true;
                // Set Code39
                config.decoderParams.code39.enabled = true;
                //Set Code128
                config.decoderParams.code128.enabled = true;
                //set inverser1Mode
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.cameraSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                config.readerParams.readerSpecific.imagerSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;


                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void setReaderParams() {
        if (scanner != null) {
            try {
                ScannerConfig config = scanner.getConfig();
                config.readerParams.readerSpecific.laserSpecific.inverse1DMode = ScannerConfig.Inverse1DMode.AUTO;
                scanner.setConfig(config);
            } catch (ScannerException e) {
                updateStatus(e.getMessage());
            }
        }
    }

    private void stopScan() {

        if (scanner != null) {
            // Reset continuous flag
            bContinuousMode = false;

            // Cancel the pending read.
            try {
                scanner.cancelRead();
            } catch (ScannerException e) {
                e.printStackTrace();
            }
        }
    }

    private void cancelRead() {
        if (scanner != null) {
            if (scanner.isReadPending()) {
                try {
                    scanner.cancelRead();
                } catch (ScannerException e) {
//                    updateStatus(e.getMessage());
                }
            }
        }
    }

    private void updateStatus(final String status) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textViewStatus.setText("" + status);
            }
        });
    }

    private void updateData(final String result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (result != null) {
                    updateSession(result);
                    String coilNo = "";
                    String size = "";
                    String netWeight = "";
                    String grossWeight = "";
                    String length = "";
                    String qualityStandard = "";
                    String classification = "";
                    String coatingMass = "";
                    String property = "";
                    String surfaceTreatment = "";
                    String dateOfProduction = "";
                    String inspector = "";

                    int index1 = result.indexOf("|");
                    int index2 = result.indexOf("|", index1 + 1);
                    int index3 = result.indexOf("|", index2 + 1);
                    int index4 = result.indexOf("|", index3 + 1);
                    int index5 = result.indexOf("|", index4 + 1);
                    int index6 = result.indexOf("|", index5 + 1);
                    int index7 = result.indexOf("|", index6 + 1);
                    int index8 = result.indexOf("|", index7 + 1);
                    int index9 = result.indexOf("|", index8 + 1);
                    int index10 = result.indexOf("|", index9 + 1);
                    int index11 = result.lastIndexOf("|");

                    coilNo = result.substring(0, index1);
                    size = result.substring(index1 + 1, index2);
                    netWeight = result.substring(index2 + 1, index3);
                    grossWeight = result.substring(index3 + 1, index4);
                    length = result.substring(index4 + 1, index5);
                    qualityStandard = result.substring(index5 + 1, index6);
                    classification = result.substring(index6 + 1, index7);
                    coatingMass = result.substring(index7 + 1, index8);
                    property = result.substring(index8 + 1, index9);
                    surfaceTreatment = result.substring(index9 + 1, index10);
                    dateOfProduction = result.substring(index10 + 1, index11);
                    dateOfProduction=dateOfProduction.substring(0,4)+"-"+dateOfProduction.substring(4,6)+"-"+dateOfProduction.substring(6,8);
                    inspector = result.substring(index11 + 1, result.length());

                    edtCoilNo.setText(coilNo);
                    edtSize.setText(size);
                    edtNetWeight.setText(netWeight);
                    edtGrossWeight.setText(grossWeight);
                    edtLength.setText(length);
                    edtQualityStandard.setText(qualityStandard);
                    edtClassification.setText(classification);
                    edtCoatingMass.setText(coatingMass);
                    edtProperty.setText(property);
                    edtSurfaceTreatment.setText(surfaceTreatment);
                    edtDateOfProduction.setText(dateOfProduction);
                    edtInspector.setText(inspector);

                    try {
                        final SessionDetail sessionDetailAuto = new SessionDetail(idSession, coilNo, Double.parseDouble(size), Double.parseDouble(netWeight), !edtGrossWeight.getText().toString().equalsIgnoreCase("-")?Double.parseDouble(grossWeight):0, Double.parseDouble(length), qualityStandard, classification, coatingMass, property, surfaceTreatment, simpleDateFormat.parse(dateOfProduction), inspector);
                        addSessionDetailNotDuplicate(sessionDetailAuto);
                        updateLv();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    private Boolean checkDuplicateCoilNo(String sessionID,String coilNo){
        return sessionDetailDAO.checkDuplicateCoilNo(sessionID,coilNo);
    }


    private void addSessionDetailNotDuplicate(SessionDetail sessionDetail) {
//        if(checkDuplicateCoilNo(sessionNumber,sessionDetail.getCoilNo())==true){
//            Toast.makeText(MainActivity3.this,"Mã số cuộn đã scan!!!!!",Toast.LENGTH_SHORT).show();
//            return;
//        }
        //add session detail
        long result = sessionDetailDAO.insert(sessionDetail);
        if (result > 0) {
            sessionDAO.update(new Session(deviceNumber, sessionNumber
                    , sessionDAO.getSumSkuBySession(sessionNumber, deviceNumber) + 1, createDate, syncStatus));
            setSumSku();
            View content = findViewById(android.R.id.content);
            Snackbar.make(content, "Success add " + sessionDetail.getCoilNo(), Snackbar.LENGTH_SHORT).show();
        }
    }

    public void updateSession(String result) {
        tvSumCoilNo.setText("Tổng số lượng đã scan: " + sumCoilNo);
        Session session = new Session();
        session = new Session(deviceNumber, sessionNumber, sumCoilNo, createDate, syncStatus);
        sessionDAO.update(session);
    }

    public void setAdapter() {
        sessionDetailAdapter2 = new SessionDetailAdapter2(MainActivity3.this, sessionDetailArrayList, new SessionDetailAdapter2.ItemClickListener() {
            @Override
            public void onClick(SessionDetail sessionDetail) {

            }
        }, new SessionDetailAdapter2.ItemSwipeListener() {
            @Override
            public void onSwipe(SessionDetail sessionDetail, boolean left, int index) {
                if(index==0){
                    edtCoilNo.setText(sessionDetail.getCoilNo());
                    edtSize.setText(sessionDetail.getSize()+"");
                    edtNetWeight.setText(sessionDetail.getNetWeight()+"");
                    edtGrossWeight.setText(sessionDetail.getGrossWeight()+"");
                    edtLength.setText(sessionDetail.getLength()+"");
                    edtQualityStandard.setText(sessionDetail.getQualityStandard());
                    edtClassification.setText(sessionDetail.getClassification());
                    edtCoatingMass.setText(sessionDetail.getCoatingMass());
                    edtProperty.setText(sessionDetail.getProperty());
                    edtSurfaceTreatment.setText(sessionDetail.getSurfaceTreatment());
                    edtDateOfProduction.setText(simpleDateFormat.format(sessionDetail.getDateOfProduction()));
                    edtInspector.setText(sessionDetail.getInspector());
                }
                else if (index ==1) {
                    deleteSessionDetailDialog(sessionDetail);
                }
            }
        });
        rvSessionDetail.setLayoutManager(new LinearLayoutManager(MainActivity3.this));
        rvSessionDetail.setAdapter(sessionDetailAdapter2);
    }

    public void deleteSessionDetailDialog(final SessionDetail sessionDetail1) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity3.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_delete_session_detail, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                updateLv();
            }
        });

        TextView txt_coil_no=alertDialog.findViewById(R.id.txt_coil_no);
        txt_coil_no.setText(sessionDetail1.getCoilNo());
        Button btnYes, btnNo;
        btnYes = alertDialog.findViewById(R.id.btnYes);
        btnNo = alertDialog.findViewById(R.id.btnNo);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int result = sessionDetailDAO.deleteByID(String.valueOf(sessionDetail1.getId()));
                if (result > 0) {
                    sessionDAO.update(new Session(deviceNumber, sessionNumber
                            , sessionDAO.getSumSkuBySession(sessionNumber, deviceNumber) -1, createDate, syncStatus));
                    setSumSku();
                    alertDialog.cancel();
                    Toast.makeText(MainActivity3.this, "Xóa thành công mã số cuộn " + sessionDetail1.getCoilNo(), Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    public void updateLv() {
        sessionDetailArrayList = sessionDetailDAO.getBySession(idSession);
        setAdapter();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home) {
            backToCreateSession();
        }
        if (id == R.id.action_settings) {
            startActivity(new Intent(MainActivity3.this, SettingActivity.class));
            return true;
        }
        if (id == R.id.action_list_session) {
            showListSession();
        }
        return super.onOptionsItemSelected(item);
    }

    public void showListSession() {
        Intent intent = new Intent(MainActivity3.this, SessionActivity.class);
        intent.putExtra(MainActivity3.DEVICE_NUMBER, deviceNumber);
        intent.putExtra(MainActivity3.SESSION_NUMBER, sessionNumber);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (true) {
            backToCreateSession();
        } else {
            super.onBackPressed();
            return;
        }

    }

    public void backToCreateSession() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity3.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.dialog_logout_scan_screen, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        Button btnYes, btnNo;
        btnYes = alertDialog.findViewById(R.id.btnYes);
        btnNo = alertDialog.findViewById(R.id.btnNo);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity3.this, LoginActivity.class);
                startActivity(intent);
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }
}
